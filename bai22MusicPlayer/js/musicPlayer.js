const $ = document.querySelector.bind(document);
const $$ = document.querySelectorAll.bind(document);
const heading = $("header h2");
const cdThumb = $(".cd-thumb");
const audio = $("#audio");
const cd = $(".cd");
const playBtn = $(".btn-toggle-play");
const player = $(".player");
const progress = $("#progress");
const nextBtn = $(".btn-next");
const preBtn = $(".btn-prev");
const randomBtn = $(".btn-random");
const repeatBtn = $(".btn-repeat");
const playlist =$(".playlist");
console.log(randomBtn);

const app = {
  currentIndex: 0,
  isPlaying: false,
  isRandom: false,
  isRepeat: false,
  songs: [
    {
      name: "2 phút hơn !!!",
      singer: "Pháo",
      path: "./music/2PhutHonKAIZ.mp3",
      image: "./img/2phuthon.jpg",
    },
    {
      name: "Anh mệt rồi",
      singer: "Anh Quân",
      path: "./music/AnhMetRoi.mp3",
      image: "./img/anhmetroi.jpg",
    },
    {
      name: "Ba kể con nghe",
      singer: "Châu Khải Phong",
      path: "./music/BaKeConNgheAcoustic.mp3",
      image: "./img/bakeconnghe.jpg",
    },
    {
      name: "Forever alone",
      singer: "Justatee",
      path: "./music/ForeverAlone.mp3",
      image: "./img/foreveralone.jpg",
    },
    {
      name: "Hạ Còn Vương Nắng",
      singer: "DatKaa",
      path: "./music/HaConVuongNangGuitar.mp3",
      image: "./img/haconvuongnang.jpg",
    },
    {
      name: "Lớn rồi còn khóc nhé",
      singer: "Trúc nhân",
      path: "./music/LonRoiConKhocNhe.mp3",
      image: "./img/lonroiconkhocnhe.jpg",
    },
    {
      name: "Một đêm say",
      singer: "Thịnh Suy",
      path: "./music/MotDemSay.mp3",
      image: "./img/motdemsay.jpg",
    },
    {
      name: "Phố cũ còn anh",
      singer: "Quinn ft Chilly",
      path: "./music/PhoCuConAnh.mp3",
      image: "./img/phocuconanh.jpg",
    },
  ],

  start: function () {
    // định nghĩa các thuộc tính cho Object
    this.defineProperties(),
      // lắng nghe / xử lý các sự kiện (DOM events)
      this.handleEvent(),
      // tải thông tin bài hát đầu tiên vào UI khi chạy ứng dụng
      this.loadCurrentSong();

    // render playlist
    this.render();
  },

  scrollToActiveSong: function(){
    
    if(this.currentIndex < 3){
      setTimeout(() => {
        $('.song.active').scrollIntoView({
          behavior: 'smooth',
          
          block: 'center'
        })
      }, 300);
    }else{
      setTimeout(() => {
        $('.song.active').scrollIntoView({
          behavior: 'smooth',
          
          block: 'nearest'
        })
      }, 300);
    }
    
  },

  loadCurrentSong: function () {
    heading.textContent = this.currentSong.name;
    cdThumb.style.backgroundImage = `url(${this.currentSong.image})`;
    audio.src = this.currentSong.path;
  },

  handleEvent: function () {
    const cdWidth = cd.offsetWidth;
    const _this = this;

    //xu ly CD quay / dung
    const thumbAnimate = cdThumb.animate([{ transform: "rotate(360deg)" }], {
      duration: 15000, //quay trong 10s
      iterations: Infinity,
    });
    thumbAnimate.pause();

    // xử lý phòng to thu nhỏ cd
    document.onscroll = function () {
      const scrollTop = document.documentElement.scrollTop || window.scrollY;
      const newCdWidth = cdWidth - scrollTop;
      cd.style.width = newCdWidth > 0 ? newCdWidth + "px" : 0;
      cd.style.opacity = newCdWidth / cdWidth;
    };

    // sử lý nút play
    playBtn.onclick = function () {
      if (_this.isPlaying) {
        audio.pause();
      } else {
        audio.play();
      }
    };

    // khi song dc play
    audio.onplay = function () {
      _this.isPlaying = true;
      thumbAnimate.play();
      player.classList.add("playing");
     
    };

    // khi song bi pause
    audio.onpause = function () {
      _this.isPlaying = false;
      thumbAnimate.pause();
      player.classList.remove("playing");
      
    };

    // khi tien do bai hat thay doi
    audio.ontimeupdate = function () {
      if (audio.duration) {
        const progressPercent = Math.floor(
          (audio.currentTime / audio.duration) * 100
        );
        progress.value = progressPercent;
      }
    };

    // xu ly khi tua song
    progress.onchange = function (e) {
      const seekTime = (audio.duration * e.target.value) / 100;
      audio.currentTime = seekTime;
    };

    //next song
    nextBtn.onclick = function () {
      if(_this.isRandom){
        _this.playRandom();
      }else{
        _this.nextSong();
      }
      
      audio.play();
      _this.render()
      _this.scrollToActiveSong()
    };

    //pre song
    preBtn.onclick = function () {
      if(_this.isRandom){
        _this.playRandom();
      }else{
        _this.prevSong();
      }
      
      audio.play();
      _this.render()
      _this.scrollToActiveSong()
    }; 
 
    // random song 
    randomBtn.onclick = function () { 
      //  if(_this.isRandom){ 
      //   randomBtn.classList.remove('active'); 
      //   _this.isRandom = false 
      //  }else{ 
      //   randomBtn.classList.add('active'); 
      //   _this.isRandom = true 
      //  } 
      _this.isRandom = !_this.isRandom;
      randomBtn.classList.toggle("active", _this.isRandom);
    };

    // xu ly next song khi audio ended
    audio.onended = function(){
      if(_this.isRepeat){
        audio.play();
      }else{
        nextBtn.click();
      }
      
    }

    // xu ly repeat
    repeatBtn.onclick = function(){
      _this.isRepeat = !_this.isRepeat;
      repeatBtn.classList.toggle("active", _this.isRepeat);
    }

    // lang nghe khi click vao playlish
    playlist.onclick = function(e){
      const songNode = e.target.closest('.song:not(.active)');
      
      if(songNode || e.target.closest('.option')){
          if(songNode){
            //phai ep kieu vi dong 278 currentIndex ==index
            _this.currentIndex = Number(songNode.dataset.index);
            _this.loadCurrentSong();
            _this.render();
            audio.play();
          }

          // xu ly khi click vao song option
          if(e.target.closest('.option')){

          }
      }
    }
  },

  nextSong: function () {
    this.currentIndex++;
    if (this.currentIndex >= this.songs.length) {
      this.currentIndex = 0;
    }
    this.loadCurrentSong();
  },

  prevSong: function () {
    this.currentIndex--;
    if (this.currentIndex < 0) {
      this.currentIndex = this.songs.length-1;
    }
    this.loadCurrentSong();
  },

  playRandom: function(){
    
    let newIndex;
    
    do {
      newIndex = Math.floor(Math.random() * this.songs.length)
    } while (newIndex === this.currentIndex);
    this.currentIndex = newIndex;
    this.loadCurrentSong();
  },

  render: function () {
    const htmls = this.songs.map((song, index) => {
      return `<div class="song ${index === this.currentIndex ? 'active':''}" data-index="${index}">
           <div class="thumb" style="background-image: url('${song.image}')">
           </div>
           <div class="body">
             <h3 class="title">${song.name}</h3>
             <p class="author">${song.singer}</p>
           </div>
           <div class="option">
             <i class="fas fa-ellipsis-h"></i>
           </div>
         </div>`;
    });

    playlist.innerHTML = htmls.join("");
  },

  defineProperties: function () {
    Object.defineProperty(this, "currentSong", {
      get: function () {
        return this.songs[this.currentIndex];
      },
    });
  },
};

app.start();
